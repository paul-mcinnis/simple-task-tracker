import React from 'react';
import { Button } from 'react-native'
import { EditTask, Home, NewTask, TaskInfo} from './screens/index';
import { StackNavigator, } from 'react-navigation';

const App = StackNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      header: null,
   },
  },
  EditTask: {
    screen: EditTask,
    navigationOptions: {
      headerBackTitle: "back",
      title: "Edit Task",
    },
  },
  NewTask: {
    screen: NewTask,
    navigationOptions: {
      headerBackTitle: "back",
      title: "New Task",
    },
  },
  TaskInfo: {
    screen: TaskInfo,
    navigationOptions: {
      title: "Task Info",
      headerRight: <Button title="Info" />
    }
  }
});

export default App;
