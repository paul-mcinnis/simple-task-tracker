import { Dimensions, StyleSheet } from 'react-native';

const screenW = Dimensions.get('window').width;
const screenH = Dimensions.get('window').height;

const styles = StyleSheet.create({
  icon: {
    
  },
  iconContainer: {
    position: 'absolute',
    marginLeft: screenW - 75,
    marginTop: screenH - 75,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red',
    height:60,
    width: 60,
    borderRadius: 30
  },
  main: {
    flex: 1,
  },
  taskContainer: {
    marginBottom: 15,
    borderWidth: 1,
    backgroundColor: 'white'
  },
  taskListContainer: {
    flex:1,
    alignItems: 'center',
    marginTop: screenH/20,
    backgroundColor: 'transparent'
  }
});

export { styles };