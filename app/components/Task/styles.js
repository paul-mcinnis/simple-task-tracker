import { Dimensions, Modal, StyleSheet } from 'react-native';

const screenW = Dimensions.get('window').width;
const screenH = Dimensions.get('window').height;

const styles = StyleSheet.create({
  buttonContainer: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'space-between'
  },
  container: {
    marginBottom: 10,
    borderWidth: 2,
    borderRadius: 5,
    width: screenW * 0.8,
    marginRight: screenW * 0.1,
  },
  date: {
    marginLeft: 5,
    marginBottom: 5,
    color: 'darkblue'
  },
  icon: {
    
  },
  iconContainer: {
    backgroundColor: 'grey',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 5,
    paddingTop: 3,
    paddingBottom: 3,
    paddingRight: 5,
    marginTop: -3,
    marginBottom: -3,
    marginRight: -3,
    borderTopLeftRadius: 30,
    borderBottomLeftRadius: 30,
    borderWidth: 2
  },
  modalContainer: {
    width: screenW * 0.9,
    height: screenH * 0.9,
    marginTop: screenH / 20,
    marginLeft: screenW * 0.05,
    backgroundColor: 'white',
    borderRadius: 10,
    borderWidth: 2
  },
  swipeout: {
    borderRadius: 5,
    backgroundColor: 'white'
  },
  title: {
    margin: 5,
    fontWeight: 'bold',
    fontSize: 16
  }
});

export {styles};