import React, { Component, } from 'react'
import { 
  Alert, 
  Text, 
  Modal,
  TouchableHighlight,
  TouchableWithoutFeedback, 
  View 
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import Swipeout from 'react-native-swipeout'
import { styles } from './styles'

  var swipeoutBtns = [{
            text: 'edit',
            type: 'primary',
            color: 'black',
            backgroundColor: 'grey',
            onPress: this.editTask
          },
          {
            text: 'delete',
            type: 'secondary',
            color: 'white',
            backgroundColor: 'darkred',
            onPress: this.deleteTask
          }
    ] 

class Task extends Component {

  constructor(props) {
    super(props);
  }
  
  render(){
    var date = this.props.date;
    if (date != ""){
      date = moment(this.props.date).format('ddd Do MMM');
    }
    const { navigate } = this.props.navigation
    return(
      <View> 
      <View style={styles.container}>
        <Swipeout
          style={styles.swipeout}
          right={swipeoutBtns}
          >
          <TouchableWithoutFeedback
            onPress={() => navigate('TaskInfo')}>
            <View style={styles.buttonContainer}>
              <View>
                <Text style={styles.title}>{this.props.title}</Text>
                <Text style={styles.date}>{date}</Text>
              </View>
              <View style={styles.iconContainer}>
                <Icon style={styles.icon} name="arrow-left" size={30} color='#162129' />
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Swipeout>
      </View>
      </View>
    );
  }
  
  deleteTask (){
    Alert.alert('Error delete function not written!');
    // deleteTask
  }
  
  editTask (){
    Alert.alert('Edit the Task Here');
    // deleteTask
  }
}

export default Task