import React, { Component, } from 'react'
import {
  Button,
  FlatList,
  ImageBackground, 
  View 
} from 'react-native'
import { NewTaskButton, Task} from '../components/index'
import { styles } from './styles'
import { tasks } from './tasks'

class Home extends Component {
  
  render() {
    return (
      <View style={styles.main}>
        <ImageBackground source={require('../media/background.jpeg')} style={styles.background}>
          <TaskList navigation={this.props.navigation}/>
          <NewTaskButton navigation={this.props.navigation}/>
        </ImageBackground> 
      </View>
    )
  }
}

class TaskList extends Component {
  render(){
    return (
      <View style={styles.taskListContainer}>
        <FlatList
          data= {tasks}
          renderItem={({item}) => 
            <Task navigation={this.props.navigation} title={item.task} desc={item.desc} date={item.date}/>}
        />
      </View>
    );
  }
}


export default Home