import React, { Component, } from 'react'
import { View, Text} from 'react-native'

class NewTask extends Component {

  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex:1, alignItems: 'center', justifyContent: 'center'}}>
        <Text> New Task Component </Text>
      </View>
    )
  }
}

export default NewTask