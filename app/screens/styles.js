import { Dimensions, StyleSheet } from 'react-native';

const screenW = Dimensions.get('window').width;
const screenH = Dimensions.get('window').height;

const styles = StyleSheet.create({
  background: {
    height: screenH,
    width: screenW
  },
  main: {
    flex: 1,
  },
  taskListContainer: {
    flex:1,
    alignItems: 'center',
    marginTop: screenH/20,
    backgroundColor: 'transparent'
  }
});

export { styles };